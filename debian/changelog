deb-gview (0.3.0) unstable; urgency=medium

  * AUTHORS, NEWS: Update for GTK3 port and new maintainer.
  * configure.ac:
    + Check for gtk+3.0 over gtk+2.0.
    + Update version to 0.3.0.
  * src/callbacks.c: Port for GTK3 (Closes: #967307).
  * src/callbacks.h: Port for GTK3.
  * src/dvpreview.c: Port for GTK3.
  * src/interface.c:
    + Port for GTK3.
    + Add myself to About and Credits.
  * src/support.c: Port for GTK3.
  * debian/control:
    + Change dependency from libgtk2.0-dev to libgtk-3-dev.
    + Bump Standards-Version to 4.6.2.
  * debian/copyright: Add myself and update year to 2023.

 -- Josef Schneider <josef81sch@gmail.com>  Wed, 11 Jan 2023 15:38:50 +0100

deb-gview (0.2.11.3) unstable; urgency=medium

  * Adopt package (Closes: #835906)
  * debian/control: Bump Standards-Version to 4.6.1.
  * debian/copyright: Add 2022 line.

  [ Boyuan Yang ]
  * debian/changelog: Try to fix format.

 -- Josef Schneider <josef81sch@gmail.com>  Tue, 15 Nov 2022 15:04:23 +0100

deb-gview (0.2.11.2) unstable; urgency=high

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends: Drop versioned constraint on intltool, libarchive-dev and
      libglib2.0-dev.

  [ Boyuan Yang ]
  * configure.ac: Refresh to fix FTBFS (Closes: #998508)
  * debian/control: Bump Standards-Version to 4.6.0.

 -- Boyuan Yang <byang@debian.org>  Sun, 07 Nov 2021 13:57:07 -0500

deb-gview (0.2.11.1) unstable; urgency=medium

  * QA upload.
  * Orphan the package.
  * Refresh packaging:
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.5.1.
    + Migrate from cdbs to dh.
  * debian/control: Add Vcs-* fields for Debian Salsa GitLab.
  * debian/copyright: Rewrite using machine-readable format.
  * po/nl.po: Update translation. (Closes: #782394)
  * debian/control: Add missing dependency sensible-utils.

 -- Boyuan Yang <byang@debian.org>  Tue, 17 Aug 2021 22:49:48 -0400

deb-gview (0.2.11) unstable; urgency=medium

  * Move to git and update VCS fields
  * Fix ordering around AC_CONFIG_AUX_DIR to ensure translations are
    consistently installed during the build.

 -- Neil Williams <codehelp@debian.org>  Sat, 30 Aug 2014 18:14:52 -0700

deb-gview (0.2.10) unstable; urgency=low

  * Move to read filters for libarchive 3.1 and wrap read_free
    to retain support for older versions. (Closes: #701205)
  * Add support for data.xz as well as data.gz inside the .deb format.
  * Drop -Werror unless enabled with the configure option.

 -- Neil Williams <codehelp@debian.org>  Sun, 26 May 2013 12:45:04 +0100

deb-gview (0.2.9) unstable; urgency=low

  * Avoid deprecated Glib functions. (Closes: #656797)
  * Cleanup pixmap load functions

 -- Neil Williams <codehelp@debian.org>  Sat, 31 Mar 2012 23:08:00 +0100

deb-gview (0.2.8) unstable; urgency=low

  * Migrate to using libarchive 3.0.2. Patch by Andres Mejia
    <mcitadel@gmail.com>. (Closes: #653237)
  * Switch to subversion and 3.0 native with autoreconf support.
  * Bump compat to 7

 -- Neil Williams <codehelp@debian.org>  Sun, 08 Jan 2012 15:16:24 +0000

deb-gview (0.2.7) unstable; urgency=low

  * Support gcc-4.6 -Werror behaviour but retain some debug
    information. (Closes: #625321)

 -- Neil Williams <codehelp@debian.org>  Sun, 08 May 2011 15:17:35 +0100

deb-gview (0.2.6) unstable; urgency=low

  * Migrate to XDG spec for preview config file
  * Fix mangling of PACKAGE_LIBS to add gdk-pixbuf and implement
    -Wl,-z,defs -Wl,--as-needed to keep deps sane. (Closes: #554302)
  * [INTL:vi] Vietnamese program translation update (Closes: #548202)
  * [INTL:sk] Slovak translation (Closes: #572106)
  * [INTL:ru] Russian program translation (Closes: #572101)
  * [l10n:eu] updated Basque translation (Closes: #572053)
  * [INTL:sv] Updated Swedish translation (Closes: #572564)
  * [INTL:vi] Vietnamese program translation update (Closes: #548202)
  * [l10n] Updated Czech translation of deb-gview (Closes: #572938)
  * [INTL:es] Spanish translation for deb-gview (Closes: #573282)
  * [INTL:fr] French program translation update (Closes: #573785)
  * [INTL:de] German program translation update (Closes: #573831)

 -- Neil Williams <codehelp@debian.org>  Sun, 14 Mar 2010 10:10:37 +0000

deb-gview (0.2.5) unstable; urgency=low

  * Drop libgnome and libgnomeui from dependencies.
  * Drop encoding from desktop file

 -- Neil Williams <codehelp@debian.org>  Thu, 20 Aug 2009 09:12:29 +0100

deb-gview (0.2.4) unstable; urgency=low

  * New release.
  * Add format specifier for all calls to g_warning. (Closes: #530631)
  * Migrate Vcs-Browser to new alioth URL (Closes: #528296)

 -- Neil Williams <codehelp@debian.org>  Mon, 01 Jun 2009 14:43:08 +0100

deb-gview (0.2.3) unstable; urgency=low

  * Add TDeb support.
  * [INTL:eu] Add deb-gview Basque translation (Closes: #518946)
  * [l10n] Updated Czech translation of deb-gview (Closes: #520757)
  * [l10n] Updated Russian translation of deb-gview (Closes: #521537)
  * [INTL:de] German translation for deb-gview (Closes: #521592)
  * Update Standards Version (no changes)
  * [l10n] French translation update for deb-gview (Closes: #522754)

 -- Neil Williams <codehelp@debian.org>  Mon, 06 Apr 2009 20:17:08 +0100

deb-gview (0.2.2) unstable; urgency=low

  * Revise handling of new .changes files for dpkg-dev 1.14.18

 -- Neil Williams <codehelp@debian.org>  Tue, 22 Apr 2008 14:21:49 +0100

deb-gview (0.2.1) unstable; urgency=low

  * Handle new format .changes files and correct
   the handling of files in the current working dir.

 -- Neil Williams <codehelp@debian.org>  Sun, 30 Mar 2008 15:44:01 +0100

deb-gview (0.2.0) unstable; urgency=low

  * New main release, migrating from libgnomevfs to GIO from
  glib-2.0
  * control: Fix the CVS location to allow debcheckout to work
  * FTBFS: support.h:39:1: error: "Q_" redefined (Closes: #471611)
  (use glib support instead).
  * tidy up unneeded files in debian/ and shorten debian/rules
  * Add gvfs-backends to Depends:
  * Add gvfs to Depends to prevent problems with incomplete GVfs
    installs.
  * Update copyright years
  * Explicitly build depend on pkgconfig instead of assuming something
    else brings it in.

 -- Neil Williams <codehelp@debian.org>  Sun, 23 Mar 2008 19:13:11 +0000

deb-gview (0.1.5) unstable; urgency=low

  * New upstream version
  * Eliminate extra dependencies by trimming pkgconfig --libs.
  * Update standards version (no changes)
  * please spell GNOME instead of Gnome (Closes: #456495)

 -- Neil Williams <codehelp@debian.org>  Tue, 01 Jan 2008 16:07:22 +0000

deb-gview (0.1.4) unstable; urgency=low

  * [INTL:fr] French program translation update (Closes: #438089)
  * coypright: Migrate to GPLv3
  * debian/copyright: use some machine-operable tags

 -- Neil Williams <codehelp@debian.org>  Sat, 08 Sep 2007 23:22:23 +0100

deb-gview (0.1.3) unstable; urgency=low

  * New upstream release
  * clarify documentation of multi-window usage (Closes: #435071)
  * [INTL:pt] Updated Portuguese translation for debconf messages
    (Closes: #435508)
  * [l10n] Updated Czech translation of deb-gview (Closes: #436467)
  * [INTL:nl] updated Dutch  translation (Closes: #436858)
  * [INTL:de] initial German debconf translation (Closes: #436952)
  * [INTL:ru] Russian program translation (Closes: #436066)
  * update to debian menu changes and verify desktop file.

 -- Neil Williams <codehelp@debian.org>  Thu, 09 Aug 2007 19:45:16 +0100

deb-gview (0.1.2) unstable; urgency=low

  * [l10n] Czech translation of deb-gview (Closes: #424118)
  * Absorb old glade2 generated code into the main codebase
  and drop glade3 due to lack of glade3 support. Finally allows
  the old generated .c files to be cleaned up.

 -- Neil Williams <codehelp@debian.org>  Wed, 30 May 2007 11:35:29 +0100

deb-gview (0.1.1) unstable; urgency=low

  * New upload due to broken tarball from using
  cvs-buildpackage.

 -- Neil Williams <codehelp@debian.org>  Thu, 19 Apr 2007 00:09:50 +0100

deb-gview (0.1.0) unstable; urgency=low

  * New release. Include support for opening
    all packages within a .changes file.
    Add GnomeVFS support to open remote .deb,
    .udeb and .changes files.
  * deb-gview : [INTL:pt] Portuguese translation for package messages
    (Closes: #418922)
  * Also includes new sv translation, thanks to Daniel Nylander
  <po@danielnylander.se>
  * [INTL:fr] French program translation update (Closes: #419193)
  * [INTL:nl] Dutch po translation (Closes: #419704)
  * [INTL:it] Italian debconf template translation (Closes: #419772)

 -- Neil Williams <codehelp@debian.org>  Wed, 18 Apr 2007 12:11:52 +0100

deb-gview (0.0.7) unstable; urgency=low

  * [INTL:fr] French program translation update (Closes: #404969)

 -- Neil Williams <codehelp@debian.org>  Sat, 30 Dec 2006 13:30:45 +0000

deb-gview (0.0.6) unstable; urgency=low

  * New release.
  * Open dialogue always defaults to /var/cache/apt/archives
   (Closes: #390595)
  * Remove maintainer mode.

 -- Neil Williams <codehelp@debian.org>  Mon, 25 Dec 2006 23:43:09 +0000

deb-gview (0.0.5) unstable; urgency=low

  * [INTL:fr] French program translation (Closes: #386536)
  * Please add ability to show manpages (Closes: #385229)
  - Added dot-file handling for user-configurable external viewers,
    including manpages, HTML and PNG images by default.
  - suggest qiv and gedit as external viewers (sensible-browser too).

 -- Neil Williams <linux@codehelp.co.uk>  Tue, 19 Sep 2006 13:12:37 +0100

deb-gview (0.0.4) unstable; urgency=low

  * New upstream release.
  * Added menu icons (Closes: #385267)
  * Package the POT file (Closes: #384847)
  * Added the ability to show gzipped files (Closes: #385227)

 -- Neil Williams <linux@codehelp.co.uk>  Sat, 02 Sep 2006 19:59:27 +0100

deb-gview (0.0.3) unstable; urgency=low

  * Initial release. (Closes: #381221: ITP: deb-gview -- Gtk viewer for
    .deb package files and contents - Debian Bug report logs)
  - Tweak short description.

 -- Neil Williams <linux@codehelp.co.uk>  Mon, 14 Aug 2006 16:20:45 +0100

deb-gview (0.0.2) unstable; urgency=low

  * Initial alioth release.
  * Project renamed deb-gview.

 -- Neil Williams <linux@codehelp.co.uk>  Thu, 03 Aug 2006 00:11:05 +0100

dpkg-view (0.0.1) unstable; urgency=low

  * Interim local release

 -- Neil Williams <linux@codehelp.co.uk>  Sat, 29 Jul 2006 15:53:55 +0100
