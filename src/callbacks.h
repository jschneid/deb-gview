/***************************************************************************
 *            callbacks.h
 *
 *  Sat Jul 29 14:15:17 2006
 *  Copyright  2006  Neil Williams
 *  linux@codehelp.co.uk
 ****************************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef _CALLBACKS_H
#define _CALLBACKS_H

//#include <libgnome/libgnome.h>

enum
{
   PKG_COLUMN,
   SIZE_COLUMN,
   LOCATION_COLUMN,
   N_COLUMNS
};

void
on_open_deb (GtkMenuItem *menuitem, gpointer user_data);

void
on_quit_activate (GtkMenuItem *menuitem, gpointer user_data);

void
on_ok_clicked (GtkButton *button, gpointer user_data);

void
on_about_activate (GtkMenuItem *menuitem, gpointer user_data);

void
on_treeview_row_activated              (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);
void
on_dpkg_view_window_activate_default   (GtkWidget       *widget,
                                        gpointer         user_data);

gboolean
on_dpkg_view_window_destroy_event      (GtkWidget       *widget,
                                        GdkEvent        *event,
                                        gpointer         user_data);

void
on_dpkg_view_window_destroy            (GtkWidget       *object,
                                        gpointer         user_data);
#endif /* _CALLBACKS_H */

void
on_dvtoolabout_clicked                 (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_dvtoolopen_clicked                  (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_dvtoolclose_clicked                 (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_dvtoolnew_clicked                   (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_dvnew_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_dvtoolquit_clicked                  (GtkToolButton   *toolbutton,
                                        gpointer         user_data);

void
on_close_activate (GtkMenuItem * menuitem, gpointer user_data);

void
on_dvexternal_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

gboolean
on_treeview_button_press_event         (GtkWidget       *widget,
                                        GdkEventButton  *event,
                                        gpointer         user_data);

void
on_dvopen_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_dvhelp_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_dvhelptoolbar_clicked               (GtkToolButton   *toolbutton,
                                        gpointer         user_data);
