/***************************************************************************
 *            interface.h
 *
 *  Wed May 30 11:26:08 2007
 *  Copyright  2007  Neil Williams
 *  linux@codehelp.co.uk
 ****************************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* with move to glade3, the glade2 xml is not necessary -
using the generated C from glade2 from now on. */

GtkWidget* create_aboutdialog (void);
GtkWidget* create_deb_gview_window (void);
GtkWidget* create_dvpopmenu (void);
