/***************************************************************************
 *            support.h
 *
 *  Wed May 30 11:27:53 2007
 *  Copyright  2007  Neil Williams
 *  linux@codehelp.co.uk
 ****************************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
/* with move to glade3, the glade2 xml is not necessary -
using the generated C from glade2 from now on. */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>
#include <glib/gi18n.h>

/*
 * Public Functions.
 */

/*
 * This function returns a widget in a component created by Glade.
 * Call it with the toplevel widget in the component (i.e. a window/dialog),
 * or alternatively any widget in the component, and the name of the widget
 * you want returned.
 */
GtkWidget*  
lookup_widget (GtkWidget       *widget,
               const gchar     *widget_name);

/*
 * Private Functions.
 */

/* This is used to create the pixmaps used in the interface. */
GtkWidget*
create_pixmap (GtkWidget       *widget,
               const gchar     *filename);

/* This is used to create the pixbufs used in the interface. */
GdkPixbuf*
create_pixbuf (const gchar     *filename);

/* This is used to set ATK action descriptions. */
void
glade_set_atk_action_description (AtkAction       *action,
                                  const gchar     *action_name,
                                  const gchar     *description);
