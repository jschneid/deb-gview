/********************************************************************
 *            dvarchive.h
 *
 *  Tue Aug  1 00:51:23 2006
 *  Copyright  2006, 2007  Neil Williams
 *  linux@codehelp.co.uk
 ********************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef _ARCHIVE_H
#define _ARCHIVE_H

#define DVCONTENTS "context"
#include <glib.h>
#include <gio/gio.h>
#include <gtk/gtk.h>

void
set_dv_parent (GtkWidget *parent);

void
dv_archive_preload (GtkWidget *widget);

typedef struct
{
	GtkTreeStore *store;
	GtkTreeIter *parent_iter;
	GtkTreeIter *child_iter;
} DVIter;

typedef struct DV_s DVContents;

DVContents *
dv_create (void);

void
open_deb (DVContents * deb);

GtkWidget *
dv_get_parent (DVContents *deb);

void
dv_parse_changes (GFile * cfile, DVContents * deb);

void
set_deb_file (GFile * gfile, DVContents *deb);

void
dv_add_child (GtkWindow * child);

guint
dv_count_kids (void);

void
dv_remove_child (GtkWindow * child);

void
dv_archive_get_control (DVIter *dvi, DVContents *deb);

void
dv_archive_get_data (DVIter *dvi, DVContents *deb);

void
dv_archive_free (DVContents *deb);

void
dv_archive_clear (DVContents *deb);

gchar *
dv_get_control_content (const gchar * file, GtkWidget * widget);

gchar *
dv_get_data_content (const gchar * file, GtkWidget * widget);

gboolean
quit_last_window (gpointer data);

const gchar *
dv_get_selected_file (DVContents * deb);

gchar *
dv_get_working_path (DVContents * deb);

void
dv_set_working_path (DVContents * deb, gchar * path);

gpointer
dv_get_raw_content (DVContents * deb);

gint64
dv_get_content_length (DVContents * deb);

void
dv_set_needs_uri (DVContents * deb, gboolean val);

gboolean
dv_get_needs_uri (DVContents * deb);

#endif /* _ARCHIVE_H */
