/***************************************************************************
 *            interface.c
 *
 *  Wed May 30 11:25:22 2007
 *  Copyright  2007  Neil Williams
 *  linux@codehelp.co.uk
 ****************************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* with move to glade3, the glade2 xml is not necessary -
using the generated C from glade2 from now on. */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"

#define GLADE_HOOKUP_OBJECT(component,widget,name) \
	g_object_set_data_full (G_OBJECT (component), name, \
	g_object_ref (widget), (GDestroyNotify) g_object_unref)

#define GLADE_HOOKUP_OBJECT_NO_REF(component,widget,name) \
	g_object_set_data (G_OBJECT (component), name, widget)

GtkWidget* create_aboutdialog (void) {
	GtkWidget *aboutdialog;
	const gchar *authors[] = {
		"Upstream author:",
		"Neil Williams <linux@codehelp.co.uk>",
		"Contributors:",
		"dpkg header code by ",
		"Ian Jackson <ian@chiark.greenend.org.uk>",
		"port for GTK3 by ",
		"Josef Schneider <josef81sch@gmail.com>",
		NULL
	};
	const gchar *documenters[] = {
		"Neil Williams",
		NULL
	};
	/* TRANSLATORS: Replace this string with your names, one name per line. */
	gchar *translators = _("translator-credits");
	GdkPixbuf *aboutdialog_logo_pixbuf;

	aboutdialog = gtk_about_dialog_new ();
	gtk_about_dialog_set_version (GTK_ABOUT_DIALOG (aboutdialog), VERSION);
	gtk_about_dialog_set_program_name (GTK_ABOUT_DIALOG (aboutdialog), _("deb-gview"));
	gtk_about_dialog_set_copyright (GTK_ABOUT_DIALOG (aboutdialog),
		"Copyright 2023 Josef Schneider <josef81sch@gmail.com>,\n"
		"2006-2008 Neil Williams <linux@codehelp.co.uk>,\n"
		"1994,1995 Ian Jackson <ian@chiark.greenend.org.uk>");
	gtk_about_dialog_set_comments (GTK_ABOUT_DIALOG (aboutdialog),
		_("Gtk/Gnome viewer for the contents of a .deb file"));
	gtk_about_dialog_set_license (GTK_ABOUT_DIALOG (aboutdialog),
	"  This package is free software; you can redistribute it and/or modify\n"
	"  it under the terms of the GNU General Public License as published by\n"
	"  the Free Software Foundation; either version 3 of the License, or\n"
	"  (at your option) any later version.\n"
	"\n"
	"  This program is distributed in the hope that it will be useful,\n"
	"  but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
	"  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
	"  GNU General Public License for more details.\n"
	"\n"
	"  You should have received a copy of the GNU General Public License\n"
	"  along with this program.  If not, see <http://www.gnu.org/licenses/>.\n");
	gtk_about_dialog_set_website (GTK_ABOUT_DIALOG (aboutdialog), "http://tracker.debian.org/pkg/deb-gview");
	gtk_about_dialog_set_website_label (GTK_ABOUT_DIALOG (aboutdialog), _("Homepage:"));
	gtk_about_dialog_set_authors (GTK_ABOUT_DIALOG (aboutdialog), authors);
	gtk_about_dialog_set_documenters (GTK_ABOUT_DIALOG (aboutdialog), documenters);
	gtk_about_dialog_set_translator_credits (GTK_ABOUT_DIALOG (aboutdialog), translators);
	aboutdialog_logo_pixbuf = create_pixbuf ("deb-gview.xpm");
	gtk_about_dialog_set_logo (GTK_ABOUT_DIALOG (aboutdialog), aboutdialog_logo_pixbuf);
	gtk_window_set_icon (GTK_WINDOW (aboutdialog), aboutdialog_logo_pixbuf);

	/* Store pointers to all widgets, for use by lookup_widget(). */
	GLADE_HOOKUP_OBJECT_NO_REF (aboutdialog, aboutdialog, "aboutdialog");

	return aboutdialog;
}

GtkWidget* create_deb_gview_window (void) {
	GtkWidget *deb_gview_window;
	GdkPixbuf *deb_gview_window_icon_pixbuf;
	GtkWidget *vbox1, *vbox2, *hbox6, *vbox4;
	GtkWidget *menubar1, *dvmenufile, *dvmenufile_menu;
	GtkWidget *dvnew, *open, *dvseparatorf;
	GtkWidget *quit, *close;
	GtkWidget *dfview, *dfview_menu, *dvexternal, *dvhelp, *dvhelp_menu;
	GtkWidget *dvhelpmenu, *image1, *about, *dvtoolbar;
	GtkWidget *dvtoolnew, *separatortoolitem1, *dvtoolopen, *separatortoolitem3;
	GtkWidget *dvtoolabout, *separatortoolitem2, *dvhelptoolbar, *separatortoolitem4;
	GtkWidget *dvtoolclose, *dvtoolquit, *hbox4, *separatortoolitem5;
	GtkWidget *dvhpane;
	GtkWidget *scrolledwindow1;
	GtkWidget *treeview;
	GtkWidget *scrolledwindow2;
	GtkWidget *display;
	GtkWidget *vbox3;
	GtkWidget *dvstatusbar;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	deb_gview_window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_set_size_request (deb_gview_window, 600, 400);
	gtk_window_set_title (GTK_WINDOW (deb_gview_window), _("Debian package file viewer"));
	deb_gview_window_icon_pixbuf = create_pixbuf ("deb-gview.xpm");
	if (deb_gview_window_icon_pixbuf) {
		gtk_window_set_icon (GTK_WINDOW (deb_gview_window), deb_gview_window_icon_pixbuf);
		g_object_unref (deb_gview_window_icon_pixbuf);
	}

	vbox1 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (vbox1);
	gtk_container_add (GTK_CONTAINER (deb_gview_window), vbox1);

	vbox2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (vbox2);
	gtk_box_pack_start (GTK_BOX (vbox1), vbox2, TRUE, TRUE, 0);

	hbox6 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_show (hbox6);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox6, FALSE, FALSE, 0);

	vbox4 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (vbox4);
	gtk_box_pack_start (GTK_BOX (hbox6), vbox4, TRUE, TRUE, 0);

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);
	gtk_box_pack_start (GTK_BOX (vbox4), menubar1, FALSE, FALSE, 0);

	dvmenufile = gtk_menu_item_new_with_mnemonic (_("_File"));
	gtk_widget_show (dvmenufile);
	gtk_container_add (GTK_CONTAINER (menubar1), dvmenufile);

	dvmenufile_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (dvmenufile), dvmenufile_menu);

	dvnew = gtk_menu_item_new_with_mnemonic (_("_New"));
	gtk_widget_add_accelerator (dvnew, "activate", accel_group,
		GDK_KEY_N, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_show (dvnew);
	gtk_container_add (GTK_CONTAINER (dvmenufile_menu), dvnew);

	open = gtk_menu_item_new_with_mnemonic (_("_Open"));
	gtk_widget_add_accelerator (open, "activate", accel_group,
		GDK_KEY_O, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_show (open);
	gtk_container_add (GTK_CONTAINER (dvmenufile_menu), open);

	dvseparatorf = gtk_separator_menu_item_new ();
	gtk_widget_show (dvseparatorf);
	gtk_container_add (GTK_CONTAINER (dvmenufile_menu), dvseparatorf);
	gtk_widget_set_sensitive (dvseparatorf, FALSE);

	close = gtk_menu_item_new_with_mnemonic (_("_Close"));
	gtk_widget_add_accelerator (close, "activate", accel_group,
		GDK_KEY_W, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_show (close);
	gtk_container_add (GTK_CONTAINER (dvmenufile_menu), close);

	quit = gtk_menu_item_new_with_mnemonic (_("_Quit"));
	gtk_widget_add_accelerator (quit, "activate", accel_group,
		GDK_KEY_Q, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);
	gtk_widget_show (quit);
	gtk_container_add (GTK_CONTAINER (dvmenufile_menu), quit);

	dfview = gtk_menu_item_new_with_mnemonic (_("_View"));
	gtk_widget_show (dfview);
	gtk_container_add (GTK_CONTAINER (menubar1), dfview);

	dfview_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (dfview), dfview_menu);

	dvexternal = gtk_menu_item_new_with_mnemonic (_("E_xternal"));
	gtk_widget_show (dvexternal);
	gtk_container_add (GTK_CONTAINER (dfview_menu), dvexternal);
	gtk_widget_add_accelerator (dvexternal, "activate", accel_group,
		GDK_KEY_V, GDK_CONTROL_MASK, GTK_ACCEL_VISIBLE);

	dvhelp = gtk_menu_item_new_with_mnemonic (_("_Help"));
	gtk_widget_show (dvhelp);
	gtk_container_add (GTK_CONTAINER (menubar1), dvhelp);

	dvhelp_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (dvhelp), dvhelp_menu);

	dvhelpmenu = gtk_menu_item_new_with_mnemonic (_("_Help"));
	gtk_widget_show (dvhelpmenu);
	gtk_container_add (GTK_CONTAINER (dvhelp_menu), dvhelpmenu);
	gtk_widget_add_accelerator (dvhelpmenu, "activate", accel_group,
		GDK_KEY_F1, (GdkModifierType) 0, GTK_ACCEL_VISIBLE);

	image1 = gtk_image_new_from_icon_name ("_Help", GTK_ICON_SIZE_MENU);
	gtk_widget_show (image1);

	about = gtk_menu_item_new_with_mnemonic (_("_About"));
	gtk_widget_show (about);
	gtk_container_add (GTK_CONTAINER (dvhelp_menu), about);

	dvtoolbar = gtk_toolbar_new ();
	gtk_box_pack_start (GTK_BOX (vbox4), dvtoolbar, FALSE, FALSE, 0);
	gtk_toolbar_set_style (GTK_TOOLBAR (dvtoolbar), GTK_TOOLBAR_BOTH);
	gtk_toolbar_get_icon_size (GTK_TOOLBAR (dvtoolbar));

	dvtoolnew = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("document-new", GTK_ICON_SIZE_BUTTON), _("New"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvtoolnew);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvtoolnew), _("Open a .deb package in a new view window."));

	separatortoolitem1 = (GtkWidget*) gtk_separator_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (dvtoolbar), separatortoolitem1);

	dvtoolopen = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("document-open", GTK_ICON_SIZE_BUTTON), _("Open"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvtoolopen);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvtoolopen), _("Open a .deb package in this window."));

	separatortoolitem3 = (GtkWidget*) gtk_separator_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (dvtoolbar), separatortoolitem3);

	dvtoolabout = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("help-about", GTK_ICON_SIZE_BUTTON), _("About"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvtoolabout);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvtoolabout), _("About deb-gview"));

	separatortoolitem2 = (GtkWidget*) gtk_separator_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (dvtoolbar), separatortoolitem2);

	dvhelptoolbar = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("help-contents", GTK_ICON_SIZE_BUTTON), _("Help"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvhelptoolbar);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvhelptoolbar), _("Show the deb-gview manpage."));

	separatortoolitem4 = (GtkWidget*) gtk_separator_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (dvtoolbar), separatortoolitem4);

	dvtoolclose = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("window-close", GTK_ICON_SIZE_BUTTON), _("Close"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvtoolclose);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvtoolclose), _("Close this view window."));

	separatortoolitem5 = (GtkWidget*) gtk_separator_tool_item_new ();
	gtk_container_add (GTK_CONTAINER (dvtoolbar), separatortoolitem5);

	dvtoolquit = (GtkWidget*) gtk_tool_button_new (gtk_image_new_from_icon_name ("application-exit", GTK_ICON_SIZE_BUTTON), _("Quit"));
	gtk_container_add (GTK_CONTAINER (dvtoolbar), dvtoolquit);
	gtk_tool_item_set_tooltip_text (GTK_TOOL_ITEM (dvtoolquit), _("Quit all windows."));

	gtk_widget_show_all (dvtoolbar);

	hbox4 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_widget_show (hbox4);
	gtk_box_pack_start (GTK_BOX (vbox2), hbox4, TRUE, TRUE, 0);

	dvhpane = gtk_paned_new (GTK_ORIENTATION_HORIZONTAL);
	gtk_widget_show (dvhpane);
	gtk_box_pack_start (GTK_BOX (hbox4), dvhpane, TRUE, TRUE, 0);
	gtk_paned_set_position (GTK_PANED (dvhpane), 300);

	scrolledwindow1 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow1);
	gtk_paned_pack1 (GTK_PANED (dvhpane), scrolledwindow1, FALSE, TRUE);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwindow1), GTK_SHADOW_IN);

	treeview = gtk_tree_view_new ();
	gtk_widget_show (treeview);
	gtk_container_add (GTK_CONTAINER (scrolledwindow1), treeview);
	gtk_widget_set_tooltip_text (treeview, _("Activate to view content, right click to open in an external viewer."));
	gtk_tree_view_set_reorderable (GTK_TREE_VIEW (treeview), TRUE);

	scrolledwindow2 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_show (scrolledwindow2);
	gtk_paned_pack2 (GTK_PANED (dvhpane), scrolledwindow2, TRUE, TRUE);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrolledwindow2), GTK_SHADOW_IN);

	display = gtk_text_view_new ();
	gtk_widget_show (display);
	gtk_container_add (GTK_CONTAINER (scrolledwindow2), display);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (display), FALSE);
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (display), GTK_WRAP_WORD);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (display), FALSE);

	vbox3 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_widget_show (vbox3);
	gtk_box_pack_start (GTK_BOX (vbox2), vbox3, FALSE, FALSE, 0);

	dvstatusbar = gtk_statusbar_new ();
	gtk_widget_show (dvstatusbar);
	gtk_box_pack_end (GTK_BOX (vbox3), dvstatusbar, FALSE, FALSE, 0);

	g_signal_connect ((gpointer) deb_gview_window, "show",
		G_CALLBACK (on_dpkg_view_window_activate_default), NULL);
	g_signal_connect ((gpointer) deb_gview_window, "destroy_event",
		G_CALLBACK (on_dpkg_view_window_destroy_event), NULL);
	g_signal_connect ((gpointer) deb_gview_window, "destroy",
		G_CALLBACK (on_dpkg_view_window_destroy), NULL);
	g_signal_connect ((gpointer) dvnew, "activate",
		G_CALLBACK (on_dvnew_activate), NULL);
	g_signal_connect ((gpointer) open, "activate",
		G_CALLBACK (on_open_deb), NULL);
	g_signal_connect ((gpointer) close, "activate",
		G_CALLBACK (on_close_activate), NULL);
	g_signal_connect ((gpointer) quit, "activate",
		G_CALLBACK (on_quit_activate), NULL);
	g_signal_connect ((gpointer) dvexternal, "activate",
		G_CALLBACK (on_dvexternal_activate), NULL);
	g_signal_connect ((gpointer) dvhelpmenu, "activate",
		G_CALLBACK (on_dvhelp_activate), NULL);
	g_signal_connect ((gpointer) about, "activate",
		G_CALLBACK (on_about_activate), NULL);
	g_signal_connect ((gpointer) dvtoolnew, "clicked",
		G_CALLBACK (on_dvtoolnew_clicked), NULL);
	g_signal_connect ((gpointer) dvtoolopen, "clicked",
		G_CALLBACK (on_dvtoolopen_clicked), NULL);
	g_signal_connect ((gpointer) dvtoolabout, "clicked",
		G_CALLBACK (on_dvtoolabout_clicked), NULL);
	g_signal_connect ((gpointer) dvhelptoolbar, "clicked",
		G_CALLBACK (on_dvhelptoolbar_clicked), NULL);
	g_signal_connect ((gpointer) dvtoolclose, "clicked",
		G_CALLBACK (on_dvtoolclose_clicked), NULL);
	g_signal_connect ((gpointer) dvtoolquit, "clicked",
		G_CALLBACK (on_dvtoolquit_clicked), NULL);
	g_signal_connect ((gpointer) treeview, "row_activated",
		G_CALLBACK (on_treeview_row_activated), NULL);
	g_signal_connect ((gpointer) treeview, "button_press_event",
		G_CALLBACK (on_treeview_button_press_event), NULL);

	/* Store pointers to all widgets, for use by lookup_widget(). */
	GLADE_HOOKUP_OBJECT_NO_REF (deb_gview_window, deb_gview_window, "deb_gview_window");
	GLADE_HOOKUP_OBJECT (deb_gview_window, vbox1, "vbox1");
	GLADE_HOOKUP_OBJECT (deb_gview_window, vbox2, "vbox2");
	GLADE_HOOKUP_OBJECT (deb_gview_window, hbox6, "hbox6");
	GLADE_HOOKUP_OBJECT (deb_gview_window, vbox4, "vbox4");
	GLADE_HOOKUP_OBJECT (deb_gview_window, menubar1, "menubar1");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvmenufile, "dvmenufile");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvmenufile_menu, "dvmenufile_menu");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvnew, "dvnew");
	GLADE_HOOKUP_OBJECT (deb_gview_window, open, "open");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvseparatorf, "dvseparatorf");
	GLADE_HOOKUP_OBJECT (deb_gview_window, quit, "quit");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dfview, "dfview");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dfview_menu, "dfview_menu");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvexternal, "dvexternal");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvhelp, "dvhelp");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvhelp_menu, "dvhelp_menu");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvhelpmenu, "dvhelpmenu");
	GLADE_HOOKUP_OBJECT (deb_gview_window, image1, "image1");
	GLADE_HOOKUP_OBJECT (deb_gview_window, about, "about");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolbar, "dvtoolbar");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolnew, "dvtoolnew");
	GLADE_HOOKUP_OBJECT (deb_gview_window, separatortoolitem1, "separatortoolitem1");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolopen, "dvtoolopen");
	GLADE_HOOKUP_OBJECT (deb_gview_window, separatortoolitem3, "separatortoolitem3");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolabout, "dvtoolabout");
	GLADE_HOOKUP_OBJECT (deb_gview_window, separatortoolitem2, "separatortoolitem2");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvhelptoolbar, "dvhelptoolbar");
	GLADE_HOOKUP_OBJECT (deb_gview_window, separatortoolitem4, "separatortoolitem4");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolclose, "dvtoolclose");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvtoolquit, "dvtoolquit");
	GLADE_HOOKUP_OBJECT (deb_gview_window, hbox4, "hbox4");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvhpane, "dvhpane");
	GLADE_HOOKUP_OBJECT (deb_gview_window, scrolledwindow1, "scrolledwindow1");
	GLADE_HOOKUP_OBJECT (deb_gview_window, treeview, "treeview");
	GLADE_HOOKUP_OBJECT (deb_gview_window, scrolledwindow2, "scrolledwindow2");
	GLADE_HOOKUP_OBJECT (deb_gview_window, display, "display");
	GLADE_HOOKUP_OBJECT (deb_gview_window, vbox3, "vbox3");
	GLADE_HOOKUP_OBJECT (deb_gview_window, dvstatusbar, "dvstatusbar");

	gtk_window_add_accel_group (GTK_WINDOW (deb_gview_window), accel_group);

	return deb_gview_window;
}

GtkWidget* create_dvpopmenu (void) {
	GtkWidget *dvpopmenu;
	GtkWidget *dvopen;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	dvpopmenu = gtk_menu_new ();

	dvopen = gtk_menu_item_new_with_mnemonic (_("E_xternal"));
	gtk_widget_show (dvopen);
	gtk_container_add (GTK_CONTAINER (dvpopmenu), dvopen);
	gtk_widget_add_accelerator (dvopen, "activate", accel_group,
				  GDK_KEY_V, (GdkModifierType) GDK_CONTROL_MASK,
				  GTK_ACCEL_VISIBLE);

	g_signal_connect ((gpointer) dvopen, "activate",
		G_CALLBACK (on_dvopen_activate), NULL);

	/* Store pointers to all widgets, for use by lookup_widget(). */
	GLADE_HOOKUP_OBJECT_NO_REF (dvpopmenu, dvpopmenu, "dvpopmenu");
	GLADE_HOOKUP_OBJECT (dvpopmenu, dvopen, "dvopen");

	gtk_menu_set_accel_group (GTK_MENU (dvpopmenu), accel_group);

	return dvpopmenu;
}
