/********************************************************************
 *            callbacks.c
 *
 *  Sat Jul 29 14:14:25 2006
 *  Copyright  2006, 2007  Neil Williams
 *  linux@codehelp.co.uk
 ********************************************************************/
/*
    This package is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

//#include <gnome.h>
#include "dvarchive.h"
#include "callbacks.h"
#include "interface.h"
#include "support.h"
#include "dvpreview.h"

static void dv_close_window (GtkWidget *button) {
	dv_remove_child (GTK_WINDOW(gtk_widget_get_toplevel(button)));
	gtk_widget_destroy (gtk_widget_get_toplevel(button));
}

void on_ok_clicked (GtkButton * button, gpointer data) {
	dv_close_window (GTK_WIDGET(button));
}

static gchar * get_deb (DVContents *deb) {
	GtkWidget *chooser;
	GtkFileFilter *filter;
	gchar *filename;
	gchar * current_path;

	filename = NULL;
	g_return_val_if_fail (deb, NULL);
	chooser = gtk_file_chooser_dialog_new
		(_("Choose the Debian package file to view"),
		NULL, GTK_FILE_CHOOSER_ACTION_OPEN,
		"gtk-cancel", GTK_RESPONSE_CANCEL,
		"gtk-open", GTK_RESPONSE_ACCEPT, NULL);
	current_path = dv_get_working_path (deb);
	if (current_path) {
		gchar * tmp;
		tmp = g_path_get_dirname(current_path);
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (chooser), tmp);
		g_free (tmp);
	} else {
		gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (chooser),
			"/var/cache/apt/archives");
	}
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Debian package files *.deb"));
	gtk_file_filter_add_pattern (filter, 
		"*.deb");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(chooser), filter);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Debian Installer packages *.udeb"));
	gtk_file_filter_add_pattern (filter, 
		"*.udeb");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(chooser), filter);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Debian translation packages *.tdeb"));
	gtk_file_filter_add_pattern (filter, 
		"*.tdeb");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(chooser), filter);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("Debian package changes *.changes"));
	gtk_file_filter_add_pattern (filter, 
		"*.changes");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER(chooser), filter);
	if (gtk_dialog_run (GTK_DIALOG (chooser)) == GTK_RESPONSE_ACCEPT) {
		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (chooser));
	}
	gtk_widget_destroy (chooser);
	dv_set_working_path (deb, filename);
	return filename;
}

void on_open_deb (GtkMenuItem * menuitem, gpointer user_data) {
	GFile * gfile;
	gchar *filename;
	DVContents *deb;

	deb = (DVContents*)lookup_widget (GTK_WIDGET(menuitem), "context");
	g_return_if_fail(deb);

	filename = get_deb (deb);
	if (filename) {
		gfile = g_file_new_for_path (filename);
		set_deb_file (gfile, deb);
		open_deb (deb);
		g_free (filename);
	}
}

void on_close_activate (GtkMenuItem * menuitem, gpointer user_data) {
	DVContents *deb;

	deb = (DVContents*) lookup_widget 
		(GTK_WIDGET(menuitem), DVCONTENTS);
	gtk_widget_destroy (dv_get_parent(deb));
}

void on_quit_activate (GtkMenuItem * menuitem, gpointer user_data) {
	DVContents *deb;

	deb = (DVContents*) lookup_widget 
		(GTK_WIDGET(menuitem), "context");
	dv_archive_free (deb);
	gtk_main_quit ();
}

void on_about_activate (GtkMenuItem * menuitem, gpointer user_data) {
	GtkWidget *ab;

	ab = create_aboutdialog ();
	gtk_dialog_run (GTK_DIALOG(ab));
	gtk_widget_destroy (ab);
}

void on_treeview_row_activated (GtkTreeView * treeview,
		GtkTreePath * path, GtkTreeViewColumn * column, gpointer user_data)
{
	const gchar *file;
	gchar * text;
	GValue *value;
	GtkTreeIter iter;
	GtkTreeModel *model;

	value = g_new0 (GValue, 1);
	model = gtk_tree_view_get_model (treeview);
	gtk_tree_model_get_iter (model, &iter, path);
	gtk_tree_model_get_value (model, &iter, LOCATION_COLUMN, value);
	file = g_value_get_string (value);
	if (!file) {
		return;
	}
	text = dv_get_data_content (file, GTK_WIDGET(treeview));
	if (text) {
		GtkWidget *display_area;
		GtkTextBuffer *buffer;

		display_area = lookup_widget (GTK_WIDGET(treeview), "display");
		buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_area));
		gtk_text_buffer_set_text (buffer, text, -1);
		gtk_text_view_set_buffer (GTK_TEXT_VIEW(display_area), buffer);
	} else {
		text = dv_get_control_content (file, GTK_WIDGET(treeview));
		if (text) {
			GtkWidget *display_area;
			GtkTextBuffer *buffer;
			display_area = lookup_widget (GTK_WIDGET(treeview), "display");
			buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (display_area));
			gtk_text_buffer_set_text (buffer, text, -1);
			gtk_text_view_set_buffer (GTK_TEXT_VIEW(display_area), buffer);
		}
	}
	g_free (value);
}

void
on_dpkg_view_window_activate_default (GtkWidget * widget, gpointer user_data) {
	dv_archive_preload (widget);
}

gboolean on_dpkg_view_window_destroy_event (GtkWidget * widget,
		GdkEvent * event, gpointer user_data)
{
	DVContents *deb;

	deb = (DVContents*) lookup_widget (widget, "context");
	dv_archive_free (deb);
	gtk_main_quit ();
	return FALSE;
}

void
on_dpkg_view_window_destroy (GtkWidget * object, gpointer user_data) {
	DVContents *deb;
	guint kids;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(object), 
		"context");
	dv_remove_child (GTK_WINDOW(gtk_widget_get_toplevel
		(GTK_WIDGET(object))));
	kids = dv_count_kids ();
	if (kids == 0) {
		dv_archive_free (deb);
		gtk_main_quit ();
	}
}

void
on_dvtoolabout_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	on_about_activate (NULL, NULL);
}

void on_dvtoolopen_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	GtkWidget * parent;
	gchar *filename;
	DVContents *deb;
	GFile * gfile;

	deb = (DVContents*)lookup_widget (GTK_WIDGET(toolbutton), "context");
	g_return_if_fail(deb);
	filename = dv_get_working_path (deb);
	filename = get_deb (deb);
	if (!filename) {
		return;
	}
	gfile = g_file_new_for_path (filename);
	if (!g_file_query_exists (gfile, NULL)) {
		return;
	}
	if (g_str_has_suffix(filename, ".changes")) {
		dv_parse_changes(gfile, deb);
	} else {
		parent = dv_get_parent(deb);
		dv_archive_clear (deb);
		g_object_set_data (G_OBJECT(parent), "context", deb);
		set_dv_parent (parent);
		set_deb_file (gfile, deb);
		open_deb (deb);
		g_free (filename);
	}
}

void
on_dvtoolclose_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	dv_close_window (GTK_WIDGET(toolbutton));
}

static void new_window_init (GtkWidget *widget) {
	GtkWidget * window1;
	gchar * filename;
	DVContents * original, *deb;
	GFile * gfile;

	original = (DVContents*) lookup_widget (widget, DVCONTENTS);
	/* get the working path from the current context */
	filename = dv_get_working_path (original);
	deb = dv_create ();
	/* insert it into the new context */
	dv_set_working_path (deb, filename);
	filename = get_deb(deb);
	if (!filename) {
		return;
	}
	gfile = g_file_new_for_path (filename);
	if (!g_file_query_exists (gfile, NULL)) {
		return;
	}
	if (g_str_has_suffix(filename, ".changes")) {
		dv_parse_changes(gfile, deb);
	} else {
		window1 = create_deb_gview_window ();
		g_return_if_fail (window1);
		g_object_set_data (G_OBJECT(window1), "context", deb);
		set_dv_parent (GTK_WIDGET(window1));
		gtk_widget_show (window1);
		dv_add_child (GTK_WINDOW(window1));
		set_deb_file (gfile, deb);
		open_deb (deb);
		g_free(filename);
	}
}

void on_dvtoolnew_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	new_window_init (GTK_WIDGET(toolbutton));
}

void on_dvnew_activate (GtkMenuItem *menuitem, gpointer user_data) {
	new_window_init (GTK_WIDGET(menuitem));
}

void on_dvtoolquit_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	DVContents *deb;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(toolbutton), DVCONTENTS);
	dv_archive_free (deb);
	gtk_main_quit();
}

void on_dvexternal_activate (GtkMenuItem *menuitem, gpointer user_data) {
	DVContents * deb;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(menuitem), DVCONTENTS);
	g_return_if_fail (deb);
	if (!dv_get_selected_file (deb)) {
		return;
	}
	dv_spawn_external (GTK_WIDGET(menuitem), deb);
}

gboolean
on_treeview_button_press_event (GtkWidget *widget,
								GdkEventButton  *event, gpointer user_data)
{
	DVContents *deb;

	deb = (DVContents*) lookup_widget (widget, DVCONTENTS);
	g_return_val_if_fail (deb, FALSE);
	if (!dv_get_selected_file (deb)) {
		return FALSE;
	}
	switch (event->button) {
		case 3 : {
			GtkWidget * pop;
			pop = create_dvpopmenu ();
			gtk_menu_attach_to_widget (GTK_MENU (pop), widget, NULL);
			gtk_menu_popup_at_pointer (GTK_MENU (pop), NULL);
			break;
		}
	}
	return FALSE;
}


void on_dvopen_activate (GtkMenuItem *menuitem, gpointer user_data) {
	DVContents * deb;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(menuitem), DVCONTENTS);
	g_return_if_fail (deb);
	dv_spawn_external (GTK_WIDGET(menuitem), deb);
}

void on_dvhelp_activate (GtkMenuItem *menuitem, gpointer user_data) {
	DVContents * deb;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(menuitem), DVCONTENTS);
	g_return_if_fail (deb);
	dv_show_our_manpage (deb);
}

void on_dvhelptoolbar_clicked (GtkToolButton *toolbutton, gpointer user_data) {
	DVContents * deb;

	deb = (DVContents*) lookup_widget (GTK_WIDGET(toolbutton), DVCONTENTS);
	g_return_if_fail (deb);
	dv_show_our_manpage (deb);
}
